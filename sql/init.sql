CREATE SEQUENCE seq_events
   INCREMENT 1
   START 1;

CREATE TABLE events
(
  event_id integer NOT NULL DEFAULT nextval('seq_events'::regclass),
  source character varying(40) NOT NULL,
  unit character varying(120) NOT NULL,
  regdate timestamp without time zone NOT NULL,
  value character varying(300)  NOT NULL,
  CONSTRAINT events_pk PRIMARY KEY (event_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE events
  OWNER TO postgres;

CREATE INDEX index_events
   ON events USING btree (source ASC NULLS LAST);
package com.synapse.ms.history.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigInteger;
import java.security.Timestamp;
import java.util.Date;

/**
 * Created by omist on 30.04.2017.
 */
@Entity
@Table(name = "events")
public class EventInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="SEQ_EVENTS")
    @SequenceGenerator(name="SEQ_EVENTS", sequenceName="SEQ_EVENTS", allocationSize=1)
    @Column(name = "event_id")
    private BigInteger eventId;

    @NotNull
    private String source;

    @NotNull
    private String unit;

    @NotNull
    private Date regdate;

    private String value;

    public EventInfo() {
    }

    public EventInfo(String source, String unit, Date regdate, String value) {
        this.source = source;
        this.unit = unit;
        this.regdate = regdate;
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public BigInteger getEventId() {
        return eventId;
    }

    public void setEventId(BigInteger eventId) {
        this.eventId = eventId;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Date getRegdate() {
        return regdate;
    }

    public void setRegdate(Date regdate) {
        this.regdate = regdate;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

package com.synapse.ms.history.services;

import com.synapse.ms.history.model.EventInfo;
import com.synapse.ms.history.repository.EventInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

/**
 * Created by omist on 30.04.2017.
 */
@Service
@Transactional
public class EventService {

    private EventInfoRepository eventInfoRepository;

    @Autowired
    public EventService(EventInfoRepository eventInfoRepository) {
        this.eventInfoRepository = eventInfoRepository;
    }

    public List<EventInfo> getAllEventsForPeriod(String source, String unit, Date startDate, Date endDate, int offset, int limit) {

        Pageable pageable = new PageRequest(offset, limit);

        if (unit != null) {
            return  eventInfoRepository.findAllEventsForPeriod(source, unit, startDate, endDate, pageable);
        } else {
            return  eventInfoRepository.findAllEventsForPeriod(source, startDate, endDate, pageable);
        }
    }

    public EventInfo save(EventInfo eventInfo) {
        return eventInfoRepository.save(eventInfo);
    }

    public EventInfo getEventInfoById(BigInteger eventId) {
        return eventInfoRepository.getOne(eventId);
    }
}

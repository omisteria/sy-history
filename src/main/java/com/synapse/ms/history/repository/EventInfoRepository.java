package com.synapse.ms.history.repository;

import com.synapse.ms.history.model.EventInfo;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

/**
 * Created by omist on 30.04.2017.
 */
@Repository
@Transactional
public interface EventInfoRepository extends JpaRepository<EventInfo, BigInteger> {

    @Query("select e from EventInfo e where e.source=?1 and e.unit=?2 and e.regdate >= ?3 and e.regdate <= ?4 order by regdate desc")
    List<EventInfo> findAllEventsForPeriod(String source, String unit, Date startDate, Date endDate, Pageable pageable);

    @Query("select e from EventInfo e where e.source=?1 and e.regdate >= ?2 and e.regdate <= ?3 order by regdate desc")
    List<EventInfo> findAllEventsForPeriod(String source, Date startDate, Date endDate, Pageable pageable);
}

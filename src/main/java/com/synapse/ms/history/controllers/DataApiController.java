package com.synapse.ms.history.controllers;

import com.synapse.ms.history.model.EventInfo;
import com.synapse.ms.history.services.EventService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * Created by omist on 30.04.2017.
 */
@ApiModel(value = "DataApi", description = "Data API Controller")
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(value = "/api")
public class DataApiController {

    @Autowired
    private EventService eventService;

    @ApiOperation(value = "events", httpMethod = "GET", nickname = "Get all events for period")
    @RequestMapping(value = "/events")
    @ResponseBody
    @ApiImplicitParams({
            @ApiImplicitParam(name = "source", value = "Name of subsystem", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "unit", value = "Id of device or entity", required = false, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "startDate", value = "Start date (format= yyyy-MM-dd'T'HH:mm:ss.SSS'Z')", required = true, dataType = "date", paramType = "query"),
            @ApiImplicitParam(name = "endDate", value = "End date (format= yyyy-MM-dd'T'HH:mm:ss.SSS'Z')", required = true, dataType = "date", paramType = "query"),
            @ApiImplicitParam(name = "offset", value = "Offset", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "limit", value = "Number of rows in result", dataType = "int", paramType = "query"),
    })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = List.class),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")})
    public List<EventInfo> getEventsForPeriod(
            @RequestParam(required = true) String source,
            @RequestParam(required = false) String unit,
            @RequestParam(value = "startDate", required = true) @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'") Date startDate,
            @RequestParam(value = "endDate", required = true) @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'") Date endDate,
            @RequestParam(value = "offset", required = false, defaultValue = "0") int offset,
            @RequestParam(value = "limit", required = false, defaultValue = "30") int limit
    ) {

        //Instant instant = startDate.toInstant(ZoneOffset.UTC);
        //Date date = Date.from(instant);

        return eventService.getAllEventsForPeriod(source, unit, startDate, endDate, offset, limit);
    }


}

package com.synapse.ms.history.jms;

import com.synapse.ms.history.model.EventInfo;
import com.synapse.ms.history.services.EventService;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Date;

/**
 * Created by omist on 12.04.2017.
 */
@Component
public class MessageReceiver {

    @Autowired
    private EventService eventService;

    public void onMessage(Object object) {
        ObjectMapper mapper = new ObjectMapper();

        try {
            final BaseMessage bm = mapper.readValue((byte[])object, BaseMessage.class);

            Thread t = new Thread(new Runnable() {

                @Override
                public void run() {

                    EventInfo eventInfo = new EventInfo(bm.getSender(), bm.getIdent(), new Date(), bm.getValue());
                    eventService.save(eventInfo);

                }
            });
            t.start();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

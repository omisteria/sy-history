package com.synapse.ms.history.jms;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by omist on 16.04.2017.
 */
public class BaseMessage {

    private String sender;
    private String ident;
    private String value;
    private Map<String, String> params;

    public BaseMessage() {
        params = new HashMap<String, String>();
    }

    public BaseMessage(String sender, String ident, String value) {
        this.sender = sender;
        this.ident = ident;
        this.value = value;
        params = new HashMap<String, String>();
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getIdent() {
        return ident;
    }

    public void setIdent(String ident) {
        this.ident = ident;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    @Override
    public String toString() {
        return "BaseMessage{" +
                "sender='" + sender + '\'' +
                ", ident='" + ident + '\'' +
                ", value='" + value + '\'' +
                ", params=" + params +
                '}';
    }
}
